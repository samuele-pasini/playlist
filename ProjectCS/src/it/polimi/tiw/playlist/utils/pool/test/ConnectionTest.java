package it.polimi.tiw.playlist.utils.pool.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.junit.BeforeClass;
import org.junit.Test;

import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

public class ConnectionTest {

	private static int MAX_POOL_SIZE;
	private static List<Thread> getThreads;

	@BeforeClass
	public static void init() {
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("dbConnection.properties");
			properties.load(input);
			MAX_POOL_SIZE = Integer.parseInt(properties.getProperty("MAX_POOL_SIZE"));

		} catch (IOException | NumberFormatException e) {

			e.printStackTrace();
		}
		getThreads = new ArrayList<Thread>();

		for (int i = 0; i < MAX_POOL_SIZE + 20; i++) {
			getThreads.add(new Thread(() -> threadBehavior(System.out::println)));
		}
	}
	
	private static void threadBehavior(Consumer <String> writer)
	{
		try (Connection c = ConnectionPool.getIstance().getConnection()) {
			writer.accept(" Connection id " + c.toString() + "  Total Connections: "
					+ ConnectionPool.getIstance().getTotalConnections() + "    Used Connections: "
					+ ConnectionPool.getIstance().getUsedConnections());
			Thread.sleep(5000);
		} catch (InterruptedException | SQLException e) {

			e.printStackTrace();
		}
	}

	@Test
	public void testGetConnection() {
		for (Thread t : getThreads) {
			t.start();
		}
		for (Thread t : getThreads)
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		try {
			assertEquals("Test nessuna connessione aperta", 0, ConnectionPool.getIstance().getUsedConnections());
			assertEquals("Test nessuna connessione aperta", MAX_POOL_SIZE,
					ConnectionPool.getIstance().getTotalConnections());
		} catch (SQLException e) {

			e.printStackTrace();
		}
		try {
			ConnectionPool.getIstance().destroy();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
