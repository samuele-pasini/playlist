function updateUnloggedView(unloggedRefresh) {			//upload the view
	$('#welcomeBrand').hide();
	$('#logoutNav').hide();
	$('#loginNav').show();
	$('#signinNav').show();
	$('#addPlNav').hide();
	$('#addAlbumNav').hide();
	$('#addSongNav').hide();
	if (unloggedRefresh) unloggedRefresh();
}


function updateLoggedView(token, loggedRefresh) {								//update the logged view hiding and showing components
	var payload = parseJwt(token);
	if (payload) {
		$('#welcomeBrand').text('Welcome ' + payload['sub']).show();
		$('#logoutNav').show();
		$('#loginNav').hide();
		$('#signinNav').hide();
		if (payload['role'].toUpperCase() === 'USER') {					//only for users
			$('#addPlNav').show();
			$('#addAlbumNav').hide();
			$('#addSongNav').hide();
		}
		else if (payload['role'].toUpperCase() === 'ADMIN') {			//only for admins
			$('#addAlbumNav').show();
			$('#addSongNav').show();
			$('#addPlNav').hide();
		}
		if (loggedRefresh) loggedRefresh(payload['sub']);
	}
}

function parseJwt(token) {									//jwt handilng function
	try {
		var base64Url = token.split('.')[1];
		var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
		var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
			return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
		}).join(''));
		return JSON.parse(jsonPayload);
	} catch (error) {
		logout();
		return null;
	}
};

function checkLogged(loggedRefresh, unloggedRefresh) {							//load the logged/unlogged view checking token when you enter in a new html page
	if (localStorage.getItem('token') != null)
		updateLoggedView(localStorage.getItem('token'), loggedRefresh);
	else if (sessionStorage.getItem('token') != null)
		updateLoggedView(sessionStorage.getItem('token'), loggedRefresh);
	else
		updateUnloggedView(unloggedRefresh);
}

function logout(unloggedRefresh) {
	localStorage.removeItem('token');		//clean storage
	sessionStorage.removeItem('token');
	updateUnloggedView(unloggedRefresh);
}