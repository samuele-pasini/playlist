package it.polimi.tiw.playlist.controllers.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.polimi.tiw.exceptions.InvalidReferenceException;
import it.polimi.tiw.playlist.beans.Playlist;
import it.polimi.tiw.playlist.dao.PlaylistDAO;
import it.polimi.tiw.playlist.dao.SongDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/user/getOthSongs")
public class GetOthSongs extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PlaylistDAO playlistDAO = null;
	private SongDAO songDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		playlistDAO = new PlaylistDAO();
		songDAO = new SongDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idS = request.getParameter("id");
		int id;
		try {
			id = Integer.parseInt(idS);			//int conversion
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Playlist ID");
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			Playlist playlist = playlistDAO.getPlaylist(id, conn).orElseThrow(InvalidReferenceException::new);		//get or throw
			if (!playlist.getUser().equals(request.getAttribute("username"))) {		//checks if the user is the owner of the playlist
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("Forbidden");
				return;
			}
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(new Gson().toJson(songDAO.getNotInPlaylistSongs(id, conn)));

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to load this resource");
			return;
		} catch (InvalidReferenceException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Playlist ID");
			return;
		}
	}

}
