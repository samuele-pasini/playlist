package it.polimi.tiw.playlist.utils.pool;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Samuele Pasini
 * @author Francesco Azzoni
 */
public class ConnectionPool {

	private static ConnectionPool instance = null;
	private static int INITIAL_POOL_SIZE;
	private static int MAX_POOL_SIZE;
	private String url;
	private String user;
	private String password;
	private String driver;
	private Map<CachedConnection, Boolean> pool = null; // Boolean true = available

	private void readProperties() { // read configuration file
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("dbConnection.properties");
			properties.load(input);
			url = properties.getProperty("dbUrl");
			user = properties.getProperty("dbUser");
			password = properties.getProperty("dbPassword");
			driver = properties.getProperty("dbDriver");
			MAX_POOL_SIZE = Integer.parseInt(properties.getProperty("MAX_POOL_SIZE"));
			INITIAL_POOL_SIZE = Integer.parseInt(properties.getProperty("INITIAL_POOL_SIZE"));

		} catch (IOException | NumberFormatException e) {
		}
	}

	private ConnectionPool() throws SQLException {
		readProperties();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
		}

		pool = new ConcurrentHashMap<CachedConnection, Boolean>(); // thread safe
		for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
			pool.put(createConnection(), true);
		}

	}

	private CachedConnection createConnection() throws SQLException {
		return new CachedConnection(DriverManager.getConnection(url, user, password));
	}

	public static ConnectionPool getIstance() throws SQLException {
		if (instance == null) {
			synchronized (ConnectionPool.class) {
				if (instance == null)
					instance = new ConnectionPool();
			}
		}
		return instance;
	}

	public Connection getConnection() throws SQLException {
		while (true) {
			Set<CachedConnection> keyList = pool.keySet();
			for (CachedConnection c : keyList) {
				if (pool.get(c)) {
					synchronized (c) {
						if (pool.get(c)) {
							pool.replace(c, false);
							return c;
						}
					}
				}
			}

			// If the pool scan doesn't find any available connections it will create a new
			// one if and only if the pool hasn't reached the max number of cached
			// connections.
			if (pool.size() < MAX_POOL_SIZE)
				synchronized (pool) {
					if (pool.size() < MAX_POOL_SIZE) {
						CachedConnection newConn = createConnection();
						pool.put(newConn, false);
						return newConn;
					}
				}

			// If the pool have reached the max_connections number cached the thread will
			// sleep for 1000 ms and the retry to get a connection.
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}

	public void releaseConnection(Connection connection) throws SQLException {
		pool.replace((CachedConnection) connection, true);
	}

	public String getUrl() {
		return url;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getDriver() {
		return driver;
	}

	public int getUsedConnections() {
		Collection<Boolean> values = pool.values();
		int c = 0;
		for (Boolean b : values) {
			if (!b)
				c++;
		}
		return c;
	}

	public int getFreeConnections() {
		Collection<Boolean> values = pool.values();
		int c = 0;
		for (Boolean b : values) {
			if (b)
				c++;
		}
		return c;
	}

	public int getTotalConnections() {
		return pool.size();
	}

	public void destroy() throws SQLException {
		if (!pool.isEmpty()) {
			synchronized (pool) {
				if (!pool.isEmpty()) {
					Set<CachedConnection> keyList = pool.keySet();
					for (CachedConnection c : keyList) {
						c.fullClose();
					}
					pool.clear();
				}
			}
		}
	}

}
