(function () { // avoid variables ending up in the global scope
	var albumList, songList, playlistList;		//album data
	var modalLogin, modalSignin, modalAddAlbum, modalAddPl, modalAddSong;	//modals
	var pageOrchestrator = new PageOrchestrator(); // main controller

	function AlbumList() {
		this.table = $('#albumTable');		//get components from DOM
		this.available = $('#albumPresenti');

		this.show = function () {
			var self = this;
			$.ajax({				//ajax call
				url: 'getAlbums',
				type: 'GET',
				datatype: 'json'
			})
				.done((response) => {
					self.table.empty();
					if (response.length === 0) {
						self.available.html("No Album available")
					}
					else {
						self.update(response);
					}
				})
		}

		this.update = function (list) {
			var self = this;
			modalAddSong.loadAlbumOptions();
			$.each(list, (idx, obj) => {			//albums population
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.artist).addClass('artist'),
						$('<td>').text(obj.year.year).addClass('year'),
						$('<td>').text(obj.loader).addClass('loader')
					).addClass('selectable')
					.click(() => {
						window.location.href = 'album.html?id=' + obj.id
					}))		//redirect with click
			});
		}
	}

	function SongList() {
		this.table = $('#songTable');		//get components from DOM
		this.available = $('#canzoniPresenti');

		this.show = function () {
			var self = this;
			$.ajax({				//ajax call
				url: 'getSongs',
				type: 'GET',
				datatype: 'json'
			})
				.done((response) => {
					self.table.empty();
					if (response.length === 0) {
						self.available.html("No Song available")
					}
					else {
						self.update(response);
					}
				})
		}

		this.update = function (list) {
			var self = this;
			$.each(list, (idx, obj) => {			//albums population
				//songs population
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.writer).addClass('writer'),
						$('<td>').text(obj.album.title).click(() => {
							window.location.href = 'album.html?id=' + obj.album.id;		//redirect with click
						}).addClass('album').addClass('selectable'),
						$('<td>').text(obj.length.minute.toString().padStart(2, '0') + ":" + obj.length.second.toString().padStart(2, '0')).addClass('length'),
						$('<td>').text(obj.loader).addClass('loader')
					))
			});
		}
	}

	function PlaylistList() {
		this.table = $('#playlistTable');		//get components from DOM
		this.available = $('#playlistPresenti');

		this.show = function () {
			var self = this;
			$.ajax({				//ajax call
				url: 'getPlaylists',
				type: 'GET',
				datatype: 'json'
			})
				.done((response) => {
					self.table.empty();
					if (response.length === 0) {
						self.available.html("No Playlist available")
					}
					else {
						self.update(response);
					}
				})
		}

		this.update = function (list) {
			var self = this;
			$.each(list, (idx, obj) => {		//playlist population
				$('#playlistTable').append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.name).addClass('name'),
						$('<td>').text(obj.user).addClass('creator'),
						$('<td>').text(obj.likes).addClass('likes')
					).addClass('selectable')
					.click(() => {
						window.location.href = 'playlist.html?id=' + obj.id
					}))		//redirect with click
			});
		}
	}


	function PageOrchestrator() {
		this.start = () => {
			loadTheme();
			checkLogged();

			$('.filterAlbumBox').keyup(filterAlbumTable);			//need key release for .val()
			$('.filterSongBox').keyup(filterSongTable);			//need key release for .val()
			$('.filterPlaylistBox').keyup(filterPlaylistTable);			//need key release for .val()
			$('#logoutNav').click(() => logout());

			modalLogin = new ModalLogin();			//create modals
			modalSignin = new ModalSignin();
			modalAddAlbum = new ModalAddAlbum();
			modalAddPl = new ModalAddPl();
			modalAddSong = new ModalAddSong();

			albumList = new AlbumList();
			songList = new SongList();
			playlistList = new PlaylistList();

			modalLogin.init();				//modals init
			modalSignin.init();
			modalAddAlbum.init(() => albumList.show());
			modalAddPl.init(() => playlistList.show());
			modalAddSong.init(() => songList.show());

		}

		this.refresh = () => {

			albumList.show();
			songList.show();
			playlistList.show();
		}
	}

	$(window).load(() => {							//loading function
		pageOrchestrator.start();
		pageOrchestrator.refresh();
	});
})()