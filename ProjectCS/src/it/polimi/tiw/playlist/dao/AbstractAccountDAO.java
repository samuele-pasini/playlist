package it.polimi.tiw.playlist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.exceptions.MysqlErrorNumbers;

public abstract class AbstractAccountDAO {

	// primitive operations to override: prefix do

	protected abstract String doGetLoginQuery();

	protected abstract String doGetValidQuery();

	protected abstract String doGetSigninQuery();

	// template methods
	public boolean isValid(String user, Connection conn) throws SQLException { // is an active account?
		try (PreparedStatement statement = conn.prepareStatement(doGetValidQuery())) {
			statement.setString(1, user);
			try (ResultSet result = statement.executeQuery()) {
				boolean success = false;
				while (result.next())
					success = result.getBoolean(1);
				return success;
			}
		}
	}

	public boolean login(String user, String password, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(doGetLoginQuery())) {
			statement.setString(1, user);
			statement.setString(2, password); // hashed in md5
			try (ResultSet result = statement.executeQuery()) {
				boolean success = false;
				while (result.next())
					success = result.getBoolean(1);
				return success;
			}
		}
	}
	
	//not implemented for admin, but it could be implemented in future
	public boolean signin(String user, String password, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(doGetSigninQuery())) {
			statement.setString(1, user);
			statement.setString(2, password); // hashed in md5
			statement.executeUpdate();
			return true;

		} catch (SQLException e) {
			if (e.getErrorCode() == MysqlErrorNumbers.ER_BAD_NULL_ERROR // trigger: duplicate in another table
					|| e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY) // already registered
				return false;
			else
				throw e; // dao cant handle other SQL errors
		}
	}

}
