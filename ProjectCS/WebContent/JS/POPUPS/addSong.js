ModalAddSong.prototype = getModalPrototype();
function ModalAddSong() {
	this.target = $('#addSongForm');
	this.source = 'POPUPS/addSong.html #addSongFormContent';

	this.bind = function () {
		this.message = $('#errorMessageAS');
		this.messageContainer = $('#errorMessageAS').parent();
		this.submitButton = $('#addSongBtn');
		this.closeButton = $('#closeBtnAS');
		this.upperCloseButton = $('#closeAS');
		this.dismissButton = $('#dismissAS');
		this.form = this.submitButton.closest('form');
		this.options = $('#albumOptions');
	}

	this.submit = function (refreshFunction) {
		var self = this;
		var token = false;

		if (typeof (Storage) !== "undefined") {
			if (localStorage.token)
				token = localStorage.token;
			if (sessionStorage.token)				//something wrong in local? Better without else
				token = sessionStorage.token;
		}
		if (!token) {								//no jwt, no way to add playlist
			self.closeForm();
			return;
		}

		if (!self.form.valid())		//client side validation
			return;

		var formData = new FormData(self.form[0]);
		$.ajax({										//ajax call
			url: 'admin/do_addSong',
			type: 'POST',
			data: formData,
			contentType: false,							//we need for formData
			processData: false,
			headers: { "Authorization": 'Bearer ' + token }	//jwt auth
		})
			.done((response) => {
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-success');		//change message style
				self.message.html(response);
				var url = new URLSearchParams(window.location.search);
				if (refreshFunction && (!url.has('id') || url.get('id') === formData.get('album'))) refreshFunction();
			})
			.fail((jqXHR) => {
				if (jqXHR.status === 401) {
					logout();					//remove token
					self.closeForm();
				}
				self.messageContainer.removeClass('alert-danger alert-success').addClass('alert-danger');		//change message style
				self.message.html(jqXHR.responseText);

			}).always(() => {
				self.messageContainer.show();		//show messages

			});
	}


	this.loadAlbumOptions = function () {						//we have to fill album options for song linking
		var self = this;
		$.ajax({										//ajax call
			url: 'getAlbums',
			type: 'GET',
			datatype: 'json'
		}).done((response) => {
			self.loadAlbumOptionsHP(response)
		});
	}

	this.loadAlbumOptionsHP = function (albums) {					//fill options
		var self = this;
		this.options.empty();
		if (albums.length === 0) {
			self.closeForm();
		}
		else {
			$.each(albums, function (idx, obj) {
				self.options.append(
					$('<option>').text(obj.title + ", " + obj.artist).attr('value', obj.id));
			});
		}
	}


	this.validateForm = function () {
		$.validator.addMethod("writerRegex", function (value, element) {										//custom writer validator
			return this.optional(element) || value.match(/^([\w\d](\s)?)*[\w\d]$/);								//same regex of backend
		}, "Invalid characters, writer must contains only letters and numbers and spaces");
		$.validator.addMethod("titleRegex", function (value, element) {											//custom title validator
			return this.optional(element) || value.match(/^([\w\d!\-\.\/\?](\s)?)*[\w\d!\-\.\/\?]$/);			//same regex of backend
		}, "Invalid characters, title must contains only letters, numbers, spaces and characters ! - . / ?");
		$.validator.addMethod("linkRegex", function (value, element) {											//custom link validator
			return this.optional(element) || value.match(/^[a-zA-Z0-9_\-]{11}$/);								//same regex of backend
		}, "Insert a valid Youtube ID");
		$.validator.addMethod("lengthRegex", function (value, element) {										//custom length validator 
			return this.optional(element) || (value.match(/^[0-5]?[0-9]:[0-5][0-9]$/) && !value.match(/^0?0:[0-2][0-9]$/));
		}, "Insert a valid length");

		this.form.validate({ 	// initialize the plugin
			rules: {											//rules
				title: {
					required: true,
					minlength: 2,
					maxlength: 30,
					titleRegex: true,
				},
				writer: {
					required: true,
					minlength: 2,
					maxlength: 30,
					writerRegex: true,
				},
				length: {
					required: true,
					lengthRegex: true,
				},
				link: {
					required: true,
					linkRegex: true,
				},
			},
			messages: {											//error messages
				title: {
					required: "Please enter the title",
					minlength: $.validator.format("Title too short, at least {0} characters"),
					maxlength: $.validator.format("Title too long, max {0} characters"),
				},
				writer: {
					required: "Please enter the writer",
					minlength: $.validator.format("Writer too short, at least {0} characters"),
					maxlength: $.validator.format("Writer too long, max {0} characters"),
				},
				link: {
					required: "Please enter the Youtube Song Video ID",
				},
				length: {
					required: "Please enter the length",
				},

			},
			errorPlacement: (error, element) => {				//where to place errors
				error.addClass('alert alert-danger');
				error.insertAfter(element);
			},
			wrapper: 'div',
		});
	}
}
