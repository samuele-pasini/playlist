package it.polimi.tiw.playlist.utils.test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.utils.CheckUtils;

class CheckUtilsTest {
	@Test
	public void testValidSimple() {
		List <String> names = List.of("sa","samuele", "Francesco", "DjFoCpS", "asdasd90", "AFSCASdas789", "98a Sd90");
		names.forEach((name)->assertDoesNotThrow(()->CheckUtils.validateSimpleName(name, "name")));
	}
	
	@Test
	public void testInvalidSimple() {
		List <String> names = List.of("s", "FrancescoFrancescoFrancescoFrancesco987990", ""," ", "asdasd90!", "AFSCASdas789-", "98aSd90 ", " 98aSd90" );
		names.forEach((name)->assertThrows(WrongValueException.class, ()->CheckUtils.validateSimpleName(name,"name")));
	}
	
	@Test
	public void testValidComplex() {
		List <String> names = List.of("sa","samuele", "Francesco", "DjFoCpS", "asd./a?s!d90", "AFSCASdas789", "98a Sd90", "a-sdasd90!");
		names.forEach((name)->assertDoesNotThrow(()->CheckUtils.validateComplexName(name, "name")));
	}
	
	@Test
	public void testInvalidComplex() {
		List <String> names = List.of("s", "FrancescoFrancescoFrancescoFrancesco987990", ""," ", "a[sd<asd90!,", ";AF{SCASdas789-", "98aSd90 ", " 98aSd90" );
		names.forEach((name)->assertThrows(WrongValueException.class, ()->CheckUtils.validateComplexName(name,"name")));
	}
	
	@Test
	public void testValidPassword() {
		List <String> passwords = List.of("samuele68", "Francesco2016", "DjFoCpS!-", "asd-as_d.90", "AF-SC.ASdas789", "98_aS-d901!");
		passwords.forEach((password)->assertDoesNotThrow(()->CheckUtils.validatePsw(password,password)));
	}
	
	@Test
	public void testInvalidPassword() {
		List <String> passwords = List.of("sam", "FrancescoFrancesco987", ""," ", "asdasd90!;", "AFSCAS das789-", "98aS.'d90");
		passwords.forEach((password)->assertThrows(WrongValueException.class, ()->CheckUtils.validatePsw(password,password)));
	}
	
	@Test
	public void testValidYoutube() {
		List <String> id = List.of("LRlNml-nlPU", "6IHBaHb_9TI", "9Upo4T8rabo", "WosPavyCXjQ", "rN9V63Azc3I", "BSf0i484kkA");
		id.forEach((yId)->assertDoesNotThrow(()->CheckUtils.validateYoutubeId(yId)));
	}
	
	@Test
	public void testInvalidYoutube() {
		List <String> passwords = List.of("BSf0i484kkAa", "BSf0i484kk", "BSf i484kkA","BS(0i484k?A", "BSf0!484kkA");
		passwords.forEach((yId)->assertThrows(WrongValueException.class, ()->CheckUtils.validateYoutubeId(yId)));
	}
	
	@Test
	public void testMismatchingPassword() {
		assertThrows(WrongValueException.class, ()->CheckUtils.validatePsw("password90","password190"));
	}

}
