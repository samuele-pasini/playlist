package it.polimi.tiw.playlist.utils;

import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

/**
 * We used this class to initialize and destroy connection pool when the server starts and ends execution and load secret Jwt Key
 *
 */
@WebListener
public class AppContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
			ConnectionPool.getIstance();
		} catch (SQLException e) {
		}
		JwtUtils.loadKey();
		ServletContextListener.super.contextInitialized(sce);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		try {
			ConnectionPool.getIstance().destroy();
		} catch (SQLException e) {
		}
		ServletContextListener.super.contextDestroyed(sce);
		
	}

}

