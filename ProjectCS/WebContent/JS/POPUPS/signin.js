ModalSignin.prototype = getModalPrototype();
function ModalSignin() {
	this.target = $('#signinForm');
	this.source = 'POPUPS/signin.html #signinFormContent';

	this.bind = function () {
		this.message = $('#errorMessageS');
		this.messageContainer = $('#errorMessageS').parent();
		this.submitButton = $('#signinBtn');
		this.closeButton = $('#closeBtnS');
		this.upperCloseButton = $('#closeS');
		this.dismissButton = $('#dismissS');
		this.form = this.submitButton.closest('form');
	}

	this.submit = function () {
		var self = this;
		if (!self.form.valid())		//client side validation
			return;
		var formData = new FormData(self.form[0]);
		$.ajax({										//ajax call
			url: 'do_signin',
			type: 'POST',
			data: formData,
			contentType: false,							//we need for formData
			processData: false,
		}).done(() => {
			$('#loginBtn').closest('form').find('input[name="username"]').val(formData.get('username'));	//compile login form
			$('#loginNav>a').trigger('click');				//open login form
			self.closeForm();									//close the form triggering
		}).fail((jqXHR) => {
			if (jqXHR.status === 400 || jqXHR.status === 401 || jqXHR.status === 500)	//error messages
				self.message.html(jqXHR.responseText);
			else
				self.message.html("Unexpected error");
			self.messageContainer.show();
		});
	}

	this.validateForm = function () {
		$.validator.addMethod("usernameRegex", function (value, element) {					//custom username validator
			return this.optional(element) || value.match(/^([\w\d](\s)?)*[\w\d]$/);			//same regex of backend
		}, "Invalid characters, username must contains only letters and numbers and spaces");

		$.validator.addMethod("passwordRegex", function (value, element) {					//custom password validator
			return this.optional(element) || value.match(/^[a-zA-Z0-9!_\.\-]*$/);			//same regex of backend
		}, "Invalid characters, password must contains only letters, numbers and characters ! _ . - ");

		this.form.validate({ 	// initialize the plugin
			rules: {								//rules
				username: {
					required: true,
					minlength: 2,
					maxlength: 30,
					usernameRegex: true,
				},
				password: {
					required: true,
					minlength: 4,
					maxlength: 18,
					passwordRegex: true,
				},
				conf: {
					required: true,
					equalTo: '#password',
				},
			},
			messages: {								//error messages
				username: {
					required: "Please enter your username",
					minlength: $.validator.format("Username too short, at least {0} characters"),
					maxlength: $.validator.format("Username too long, max {0} characters"),
				},
				password: {
					required: "Please enter your password",
					minlength: $.validator.format("Password too short, at least {0} characters"),
					maxlength: $.validator.format("Password too long, max {0} characters"),
				},
				conf: {
					required: "Please enter your password confirmation",
					equalTo: "Mismatching passwords",
				},

			},
			errorPlacement: (error, element) => {	//where to place errors
				error.addClass('alert alert-danger');
				error.insertAfter(element);
			},
			wrapper: 'div',
		});
	}
}
