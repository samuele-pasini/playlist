CREATE DATABASE  IF NOT EXISTS `musiccs` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `musiccs`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: musiccs
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `username` varchar(45) NOT NULL,
  `password` char(64) NOT NULL,
  `email` varchar(256) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `mail_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('Admin1','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','admin1@gmail.com'),('Admin2','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','admin2@gmail.com'),('AdminProf','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','adminProf@gmail.com');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `admin_BEFORE_INSERT` BEFORE INSERT ON `admin` FOR EACH ROW BEGIN
	if  new.username IN 
    (
		SELECT username from musiccs.user
	)
    then
    
		SET new.username=null;
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `album` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `artist` varchar(45) NOT NULL,
  `year` int DEFAULT NULL,
  `loader` varchar(45) DEFAULT NULL,
  `cover` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`title`,`artist`),
  KEY `loaderAlbum_idx` (`loader`),
  CONSTRAINT `loaderAlbum` FOREIGN KEY (`loader`) REFERENCES `admin` (`username`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,'The Dark Side of the Moon','Pink Floyd',1973,'Admin1','1.jpg'),(2,'Abbey Road','The Beatles',1969,'Admin2','2.jpg'),(3,'Nevermind','Nirvana',1991,'Admin1','3.jpg'),(4,'Sgt. Pepper\'s Lonely Hearts Club Band','The Beatles',1967,'Admin2','4.jpg'),(5,'The Wall','Pink Floyd',1979,'Admin1','5.jpg'),(6,'Born To Run','Bruce Springsteen',1975,'Admin2','6.jpg'),(7,'The Velvet Underground & Nico','Velvet Underground',1967,'Admin1','7.jpg'),(14,'Evolve','Imagine Dragons',2017,'Admin1','8.jpg'),(15,'Californication','Red Hot Chili Peppers',1999,'Admin1','15.jpg');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `album_BEFORE_INSERT` BEFORE INSERT ON `album` FOR EACH ROW BEGIN
	set new.cover =concat(
    (SELECT MAX(id)+1 from album),'.jpg');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `likepl`
--

DROP TABLE IF EXISTS `likepl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likepl` (
  `idPlaylist` int NOT NULL,
  `username` varchar(45) NOT NULL,
  PRIMARY KEY (`idPlaylist`,`username`),
  KEY `idUser_idx` (`username`),
  CONSTRAINT `idPlaylistLike` FOREIGN KEY (`idPlaylist`) REFERENCES `playlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idUser` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likepl`
--

LOCK TABLES `likepl` WRITE;
/*!40000 ALTER TABLE `likepl` DISABLE KEYS */;
INSERT INTO `likepl` VALUES (15,'paso'),(36,'paso'),(15,'tratt'),(36,'tratt'),(15,'user1'),(37,'user1'),(42,'user1');
/*!40000 ALTER TABLE `likepl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `playlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `creator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `creator_pl_UNIQUE` (`creator`,`name`),
  KEY `creatore_idx` (`creator`),
  CONSTRAINT `creatore` FOREIGN KEY (`creator`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist`
--

LOCK TABLES `playlist` WRITE;
/*!40000 ALTER TABLE `playlist` DISABLE KEYS */;
INSERT INTO `playlist` VALUES (40,'beatles forever','paso'),(36,'rock hearth','paso'),(38,'pink floyd love','tratt'),(39,'rock hearth','tratt'),(15,'summer vibes','tratt'),(41,'America sound','user1'),(37,'psycho night','user1'),(42,'Teen','user1');
/*!40000 ALTER TABLE `playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song`
--

DROP TABLE IF EXISTS `song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `song` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `idAlbum` int DEFAULT NULL,
  `length` time DEFAULT NULL,
  `writer` varchar(45) NOT NULL,
  `loader` varchar(45) NOT NULL,
  `link` char(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`idAlbum`,`title`),
  KEY `idAlbum_idx` (`idAlbum`),
  KEY `loader_idx` (`loader`),
  CONSTRAINT `idAlbum` FOREIGN KEY (`idAlbum`) REFERENCES `album` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `loader` FOREIGN KEY (`loader`) REFERENCES `admin` (`username`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song`
--

LOCK TABLES `song` WRITE;
/*!40000 ALTER TABLE `song` DISABLE KEYS */;
INSERT INTO `song` VALUES (1,'Fixing a Hole',4,'00:02:35','Paul McCartney','Admin1','UPBd8eHQqIw'),(2,'Lucy in the Sky with Diamonds',4,'00:03:28','Lennon-McCartney','Admin1','naoknj1ebqI'),(3,'Lovely Rita',4,'00:02:42','Paul McCartney','Admin2','ysDwR5SIR1Q'),(5,'Another Brick in The Wall',5,'00:09:08','Roger Waters','Admin1','5IpYOF4Hi6Q'),(6,'Hey You',5,'00:04:41','Roger Waters','Admin1','TFjmvfRvjTc'),(7,'Comfortably Numb',5,'00:06:26','Waters-Gilmour','Admin1','_FrOQC-zEog'),(8,'Here Comes the Sun',2,'00:03:06','George Harrison','Admin1','U_O1QKQCsGs'),(9,'Time',1,'00:07:01','Waters, Gilmour, Wright, Mason','Admin1','JwYX52BP2Sk'),(10,'Breathe',1,'00:02:48','Waters, Gilmour, Wright','Admin1','mrojrDCI02k'),(11,'The Great Gig in the Sky',1,'00:04:47','Wright, Torry','Admin1','cVBCE3gaNxc'),(12,'In Bloom',3,'00:04:18','Kurt Cobain','Admin2','LTyNvD8Jaos'),(13,'Smells Like Teen Spirit',3,'00:05:02','Cobain, Novoselic, Grohl','Admin2','hTWKbfoikeg'),(14,'Lithium',3,'00:04:16','Kurt Cobain','Admin2','pkcJEvMcnEg'),(15,'Come as You Are',3,'00:03:38','Kurt Cobain','Admin2','vabnZ9-ex7o'),(16,'Polly',3,'00:02:56','Cobain','Admin1','R_-lliiKDxA'),(17,'Something',2,'00:03:02','George Harrison','Admin2','UelDrZ1aFeY'),(18,'Thunder Road',6,'00:04:49','Bruce Springsteen','Admin1','YdhkaPZtQF4'),(19,'Born To Run',6,'00:04:30','Bruce Springsteen','Admin1','f3t9SfrfDZM'),(20,'Sunday Morning',7,'00:02:56','Reed, Cale','Admin1','3qK82JvRY5s'),(24,'Femme Fatale',7,'00:02:39','Lou Reed','Admin1','jog8gh40Fho'),(25,'Heroin',7,'00:07:12','Lou Reed','Admin2','6xcwt9mSbYE'),(26,'Night',6,'00:03:01','Bruce Springsteen','Admin1','EGe1bKEdEag'),(27,'Believer',14,'00:03:23','Justin Tranter','Admin1','7wtfhZwyrcc'),(29,'Whatever it Takes',14,'00:03:21','Dan Reynolds','Admin1','gOsM-DYAEhY'),(30,'Thunder',14,'00:03:07','Dan Reynolds','Admin2','wFhs7WVvuXk'),(31,'Next To Me',14,'00:03:50','Alex Da Kind','Admin1','axS6L0NX7VE'),(32,'Californication',15,'00:05:21','Anthony Kiedis','Admin1','YlUKcNNmywk'),(33,'Scar Tissue',15,'00:03:40','John Frusciante','Admin1','mzJj5-lubeM'),(34,'Otherside',15,'00:04:15','Flea','Admin1','rn_YodiJO6k'),(35,'Parallel Universe',15,'00:04:29','Flea','Admin1','VIfxDoQDaFE');
/*!40000 ALTER TABLE `song` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_playlist`
--

DROP TABLE IF EXISTS `song_playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `song_playlist` (
  `idSong` int NOT NULL,
  `idPlaylist` int NOT NULL,
  `pos` int NOT NULL,
  PRIMARY KEY (`idSong`,`idPlaylist`),
  KEY `idPlaylist_idx` (`idPlaylist`),
  CONSTRAINT `idPlaylist` FOREIGN KEY (`idPlaylist`) REFERENCES `playlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idSong` FOREIGN KEY (`idSong`) REFERENCES `song` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_playlist`
--

LOCK TABLES `song_playlist` WRITE;
/*!40000 ALTER TABLE `song_playlist` DISABLE KEYS */;
INSERT INTO `song_playlist` VALUES (1,39,1),(1,40,1),(2,15,3),(2,37,1),(2,39,0),(2,40,0),(3,39,2),(3,40,3),(5,36,0),(5,38,0),(6,37,2),(6,38,1),(7,36,3),(7,37,0),(7,38,2),(8,15,4),(8,40,2),(9,37,4),(9,38,4),(10,38,3),(12,36,1),(12,39,5),(13,15,1),(13,36,6),(13,41,1),(14,36,2),(15,36,7),(16,37,5),(16,39,4),(18,41,4),(19,41,2),(25,36,4),(26,41,0),(27,15,0),(30,15,5),(30,42,3),(31,15,2),(32,15,6),(32,41,3),(32,42,4),(33,36,5),(33,39,3),(33,42,2),(34,37,6),(34,42,0),(35,37,3);
/*!40000 ALTER TABLE `song_playlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `song_playlist_BEFORE_INSERT` BEFORE INSERT ON `song_playlist` FOR EACH ROW BEGIN
	IF(new.pos> (
					SELECT count(*)
                    FROM song_playlist
                    WHERE idPlaylist = new.idPlaylist
                    ) || new.pos<0)
	THEN SET new.idPlaylist = null;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `username` varchar(45) NOT NULL,
  `password` char(64) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('paso','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),('tratt','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'),('user1','0b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e'),('user2','6cf615d5bcaac778352a8f1f3360d23f02f34ec182e259897fd6ce485d7870d4');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `user_BEFORE_INSERT` BEFORE INSERT ON `user` FOR EACH ROW BEGIN
		if  new.username IN 
    (
		SELECT username from musiccs.admin
	)
    then
    
		SET new.username=null;
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'musiccs'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `deleteAuthEvent` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = '+01:00' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `deleteAuthEvent` ON SCHEDULE EVERY 1 DAY STARTS '2020-04-04 12:44:26' ON COMPLETION NOT PRESERVE ENABLE DO delete from user_auth where CURRENT_TIMESTAMP()> DATE_ADD(generationTime,interval 7 DAY) */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'musiccs'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07 14:50:18
