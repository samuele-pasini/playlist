package it.polimi.tiw.playlist.beans;

import java.time.LocalTime;

public class Song {

	// attributes

	private int id;
	private String title;
	private String writer;
	private LocalTime length;
	private Album album;
	private String loader;
	private String link;

	// getters and setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public LocalTime getLength() {
		return length;
	}

	public void setLength(LocalTime length) {
		this.length = length;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getLoader() {
		return loader;
	}

	public void setLoader(String loader) {
		this.loader = loader;
	}
	
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	// constructors

	public Song(int id, String title, String writer, LocalTime length, Album album, String loader, String link) { // constructor with
																										// parameters
		this.id = id;
		this.title = title;
		this.writer = writer;
		this.length = length;
		this.album = album;
		this.loader = loader;
		this.link = link;
	}

	public Song() { // constructor without parameters
		this.id = 0;
		this.title = "";
		this.writer = "";
		this.length = null;
		this.album = null;
		this.loader = "";
		this.link = "";
	}



}
