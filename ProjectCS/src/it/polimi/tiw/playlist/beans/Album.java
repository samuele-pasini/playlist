package it.polimi.tiw.playlist.beans;

import java.time.Year;
import java.util.List;

public class Album {

	// attributes

	private int id;
	private String title;
	private String coverPath;
	private Year year;
	private String artist;
	private String loader;
	private List<Song> songs;

	// getters and setters

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getcoverPath() {
		return coverPath;
	}

	public void setcoverPath(String coverPath) {
		this.coverPath = coverPath;
	}

	public Year getYear() {
		return year;
	}

	public void setYear(Year year) {
		this.year = year;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getLoader() {
		return loader;
	}

	public void setLoader(String loader) {
		this.loader = loader;
	}

	// constructors

	public Album(int id, String title, String coverPath, Year year, String artist, String loader) {// constructor with
																									// parameters

		this.id = id;
		this.title = title;
		this.coverPath = coverPath;
		this.year = year;
		this.artist = artist;
		this.loader = loader;
	}

	public Album() { // constructor without parameters
		this.id = 0;
		this.title = "";
		this.coverPath = "";
		this.year = null;
		this.artist = "";
		this.loader = "";
	}

}
