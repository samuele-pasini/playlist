package it.polimi.tiw.playlist.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Cypher and hash utils class
 * 
 * @author Samuele Pasini
 * @author Francesco Azzoni
 *
 */
public final class Cypher {
	/**
	 * Hash a string in SHA-256
	 * @param value	String to hash
	 * @return hashed String
	 */
	public static String hashSHA(String value) { // return the parameter String encrypted in SHA-256
		if (value == null)
			return null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(value.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Cypher() { // cant instantiate
	}
}
