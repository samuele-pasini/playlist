package it.polimi.tiw.playlist.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.polimi.tiw.playlist.enums.Role;

/**
 * Jwt Utils Class
 * 
 * @author Samuele Pasini
 * @author Francesco Azzoni
 *
 */
public final class JwtUtils {

	private static Key SECRET_KEY;

	/**
	 * Load the secret key from properties file
	 */
	public static void loadKey() { // read configuration file
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("jwt.properties");
			properties.load(input);
			SECRET_KEY = Keys.hmacShaKeyFor(Base64.getDecoder().decode(properties.getProperty("secretKey"))); // secret
																												// key
																												// reading
		} catch (IOException e) {
		}
	}

	/**
	 *
	 * Extract User Claim from a String containing JWT
	 * 
	 * @param jwt String token
	 * @return User Claim (if there is a valid one)
	 */
	public static Optional<String> getUser(String jwt) {
		try {
			return Optional.ofNullable((String) getParsed(jwt).getBody().get("sub"));
		} catch (JwtException e) {
			return Optional.empty();
		}
	}

	/**
	 * Extract User Claim from a String containing JWT in Authorization Header
	 * 
	 * @param req http request
	 * @return User Claim (if there is a valid one)
	 */
	public static Optional<String> getUser(HttpServletRequest req) {
		Optional<String> jwt = getToken(req);
		if (jwt.isPresent())
			return getUser(jwt.get());
		return Optional.empty();
	}

	/**
	 * Parse a String token in his claims
	 * 
	 * @param jwt String token
	 * @return parsed Token
	 * @throws JwtException invalid Token
	 */
	private static Jws<Claims> getParsed(String jwt) throws JwtException {
		return Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(jwt);
	}

	/**
	 * Exctract a token from the header Authorization of an http request
	 * 
	 * @param req http request
	 * @return Token
	 */
	private static Optional<String> getToken(HttpServletRequest req) {
		String header = req.getHeader("Authorization");
		if (header == null || !header.startsWith("Bearer "))
			return Optional.empty(); // no token
		return Optional.ofNullable(header.substring(7));
	}

	/**
	 * Create a valid token
	 * 
	 * @param sub  logged username
	 * @param role logged role
	 * @return builded token
	 */
	public static String createToken(String sub, Role role) {
		return Jwts.builder().setSubject(sub) // username setting
				.claim("role", role) // role setting
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 10)) // 10 days
				.signWith(SECRET_KEY).compact();
	}

	/**
	 * Validate a token
	 * 
	 * @param jwt String token
	 * @return is valid or not
	 */
	public static boolean isValid(String jwt) {
		try {
			Jws<Claims> jws = getParsed(jwt);
			if (jws.getBody().getExpiration().before(new Date(System.currentTimeMillis())))
				return false;
			return true;
		} catch (JwtException ex) {
			return false;
		}
	}

	/**
	 * Validate a token
	 * 
	 * @param req http request
	 * @return the request contain a valid token or not
	 */
	public static boolean isValid(HttpServletRequest req) {
		Optional<String> jwt = getToken(req);
		if (jwt.isPresent())
			return isValid(jwt.get());
		return false;
	}

	/**
	 * Extract a Role Claim from a String containing JWT
	 * 
	 * @param jwt String token
	 * @return Role Claim (if there is a valid one)
	 */
	public static Optional<Role> getRole(String jwt) {
		try {
			return Optional.ofNullable(Role.valueOf((String) (getParsed(jwt).getBody().get("role"))));
		} catch (JwtException e) {
			return Optional.empty();
		}
	}

	/**
	 * Extract a Role Claim from a String containing JWT in Authorization Header
	 * 
	 * @param req http request
	 * @return Role Claim (if there is a valid one)
	 */
	public static Optional<Role> getRole(HttpServletRequest req) {
		Optional<String> jwt = getToken(req);
		if (jwt.isPresent())
			return getRole(jwt.get());
		return Optional.empty();
	}

	private JwtUtils() { // cant instantiate
	}

}
