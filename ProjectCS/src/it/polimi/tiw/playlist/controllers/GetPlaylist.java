package it.polimi.tiw.playlist.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.polimi.tiw.exceptions.InvalidReferenceException;
import it.polimi.tiw.playlist.beans.Playlist;
import it.polimi.tiw.playlist.dao.PlaylistDAO;
import it.polimi.tiw.playlist.dao.SongDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/getPlaylist")
public class GetPlaylist extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PlaylistDAO playlistDAO = null;
	private SongDAO songDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		playlistDAO = new PlaylistDAO();
		songDAO = new SongDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idS = request.getParameter("id");  // playlist id
		int id;
		try {
			id = Integer.parseInt(idS);           // int conversion
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Playlist ID");
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			Playlist playlist = playlistDAO.getPlaylist(id, conn).orElseThrow(InvalidReferenceException::new); // get the
																											   // playlist
																											   // or throw
			playlist.setSongs(songDAO.getPlaylistSongs(id, conn));							 //fill playlist songs
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(new Gson().toJson(playlist));

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to load this resource");
			return;
			
		} catch (InvalidReferenceException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Playlist ID");
			return;
		}
	}

}
