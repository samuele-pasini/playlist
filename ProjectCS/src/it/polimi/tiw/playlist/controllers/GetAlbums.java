package it.polimi.tiw.playlist.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import it.polimi.tiw.playlist.dao.AlbumDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;


@WebServlet("/getAlbums")
public class GetAlbums extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AlbumDAO albumDAO = null;
	
	@Override
	public void init() throws ServletException {
		super.init();
		albumDAO = new AlbumDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(new Gson().toJson(albumDAO.getAllAlbums(conn)));

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to load this resource");
			return;
		}
		
	}

}
