package it.polimi.tiw.playlist.controllers.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.exceptions.InvalidReferenceException;
import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.beans.Playlist;
import it.polimi.tiw.playlist.dao.PlaylistDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/user/do_sortSongPl")
public class SortSongPl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PlaylistDAO playlistDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		playlistDAO = new PlaylistDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idS = request.getParameter("id");
		String idSongS = request.getParameter("songId");
		String endPosS = request.getParameter("endPos");
		String startPosS = request.getParameter("startPos");
		int id, idSong, endPos, startPos;
		try {
			id = Integer.parseInt(idS); // int conversion
			if (id < 0)
				throw new WrongValueException("The playlist id must be positive");

			idSong = Integer.parseInt(idSongS);
			if (idSong < 0)
				throw new WrongValueException("The song id must be positive");

			startPos = Integer.parseInt(startPosS);
			if (startPos < 0)
				throw new WrongValueException("The start position must be positive");

			endPos = Integer.parseInt(endPosS);
			if (endPos < 0)
				throw new WrongValueException("The end position must be positive");
		} catch (NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid param values");
			return;
		} catch (WrongValueException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(e.getMessage());
			return;
		}

		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			Playlist playlist = playlistDAO.getPlaylist(id, conn).orElseThrow(InvalidReferenceException::new); // get or
																												// throw
			if (!playlist.getUser().equals(request.getAttribute("username"))) { // logged is creator?
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("Forbidden");
				return;
			}
			
			if(!playlistDAO.isSortSongValid(idSong, id, startPos, endPos, conn)) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("The song is not in this playlist or the given positions are wrong");
				return;
			}
			// endPos==startPos doesn't happen normally but is better to consider that
			if (endPos > startPos)
				playlistDAO.moveSongForward(idSong, id, startPos, endPos, conn);
			else if (endPos < startPos)
				playlistDAO.moveSongBack(idSong, id, startPos, endPos, conn);
			response.setStatus(HttpServletResponse.SC_OK);

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to load this resource");
			return;
		} catch (InvalidReferenceException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Invalid Playlist ID");
			return;
		}

	}

}
