(function () { // avoid variables ending up in the global scope
	var playlist;		//playlist data
	var player;		//youtube player
	var modalLogin, modalSignin, modalAddAlbum, modalAddPl, modalAddSong;	//modals
	var pageOrchestrator = new PageOrchestrator(); // main controller

	function Playlist() {
		this.name = $('#plName');		//get components from DOM
		this.creator = $('#plCreator');
		this.likes = $('#plLikes');
		this.table = $('#songTable');

		this.show = function () {
			var self = this;
			var url = new URLSearchParams(window.location.search);
			if (url.has('id')) {			//get playlist id
				$.ajax({					//ajax call
					url: 'getPlaylist',
					type: 'GET',
					data: { 'id': url.get('id') },
					datatype: 'json'
				})
					.done((response) => {
						if (response.length === 0)
							window.location.href = "homePage.html";		//redirect
						else
							self.update(response);

						feather.replace();								//youtube icon
					})
					.fail(() => {										//redirect
						window.location.href = "homePage.html";
					})
			}
			else
				window.location.href = "homePage.html";					//redirect
		}

		this.update = function (playlist) {
			var self = this;
			self.name.html(playlist.name);			//fill  Data
			self.creator.html(playlist.user);
			self.likes.html(playlist.likes);
			self.table.empty();						//songs population
			$.each(playlist.songs, (idx, obj) => {
				player.queue[idx] = obj.link;						//queue generation
				self.table.append(
					$('<tr>').append(
						$('<td>').text(obj.id),
						$('<td>').text(obj.title).addClass('title'),
						$('<td>').text(obj.writer).addClass('writer'),
						$('<td>').text(obj.album.title).addClass('album'),
						$('<td>').text(obj.length.minute.toString().padStart(2, '0') + ":" + obj.length.second.toString().padStart(2, '0')).addClass('length'),
						$('<td>').text(obj.loader).addClass('loader'),
						$('<td>').html('<i data-feather="youtube"></i>').addClass('icon')
					).attr({
						'data-link': obj.link
					}).click(() => {								//youtube song choosing
						player.playingIndex = player.queue.indexOf(obj.link);
						player.play(obj.link)
					}))		//bind playing function
			});
			checkLogged((name) => pageOrchestrator.updatePlaylistLoggedView(name), () => pageOrchestrator.updatePlaylistUnloggedView());
		}

		this.addLike = function () {
			var self = this;
			var url = new URLSearchParams(window.location.search);
			var token = false;
			if (typeof (Storage) !== "undefined") {
				if (localStorage.token)
					token = localStorage.token;
				if (sessionStorage.token)				//something wrong in local? Better without else
					token = sessionStorage.token;
			}
			if (!token) {								//no jwt, you cant like
				pageOrchestrator.likeBtn.attr("data-content", "You have to log in!").popover('show');;
				return;
			}

			if (url.has('id')) {			//get playlist id
				$.ajax({					//ajax call
					url: 'user/do_addLike',
					type: 'POST',
					data: { 'id': url.get('id') },
					headers: { "Authorization": 'Bearer ' + token }	//jwt auth
				})
					.done((response) => {
						pageOrchestrator.likeBtn.attr("data-content", "Liked!").popover('show');	//popover handling
						self.likes.html(parseInt((self.likes).html()) + 1);		//it doesn't need server call, if it's done, it's +1							//youtube icon
					})
					.fail((jqXHR) => {
						if (jqXHR.status === 400)
							pageOrchestrator.likeBtn.attr("data-content", jqXHR.responseText);
						else if (jqXHR.status === 401) {
							logout();							//remove token
							pageOrchestrator.likeBtn.attr("data-content", "You have to log in!");
						} else if (jqXHR.status === 403)//Forbidden
							pageOrchestrator.likeBtn.attr("data-content", "You have to log as a user!");
						else
							pageOrchestrator.likeBtn.attr("data-content", "Unexpected error");
						pageOrchestrator.likeBtn.popover('show');
					})
			}
			else
				window.location.href = "homePage.html";					//redirect
		}

	}

	function PageOrchestrator() {
		this.likeBtn = $("#likeBtn");
		this.modBtn = $("#modBtn");

		this.start = () => {
			loadTheme();

			playlist = new Playlist();
			player = new Player();

			this.likeBtn.click(() => playlist.addLike());		//like button binding
			this.modBtn.click(() => {		//composer redirect binding
				var url = new URLSearchParams(window.location.search);
				if (url.has('id')) {
					window.location.href = "composer.html?id=" + url.get("id");
				}
			})
			this.likeBtn.popover({				//popover handlig
				trigger: 'focus'
			})

			$('.filterSongBox').keyup(filterSongTable);			//need key release for .val()
			$('#logoutNav').click(
				() => logout(
					() => { this.updatePlaylistUnloggedView() }
				)
			);

			modalLogin = new ModalLogin((name) => this.updatePlaylistLoggedView(name));			//create modals
			modalSignin = new ModalSignin();
			modalAddAlbum = new ModalAddAlbum();
			modalAddPl = new ModalAddPl();
			modalAddSong = new ModalAddSong();

			modalLogin.init();				//modals init
			modalSignin.init();
			modalAddAlbum.init();
			modalAddPl.init();
			modalAddSong.init()

			feather.replace();				//youtube icon
		}

		this.refresh = function () {
			modalAddSong.loadAlbumOptions();

			playlist.show();
			player.show();
		}

		this.updatePlaylistLoggedView = function (name) {					//modify button handling
			if (playlist.creator.html() === (name))
				this.modBtn.show();
			else
				this.modBtn.hide();
		}

		this.updatePlaylistUnloggedView = function () {
			this.modBtn.hide();
		}
	}

	$(window).load(() => {							//loading function
		pageOrchestrator.start();
		pageOrchestrator.refresh();
	});
})()

