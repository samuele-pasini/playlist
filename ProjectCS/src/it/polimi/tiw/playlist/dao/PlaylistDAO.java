package it.polimi.tiw.playlist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.mysql.cj.exceptions.MysqlErrorNumbers;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.beans.Playlist;

public class PlaylistDAO {

	// queries
	private static final String GET_ALL_QUERY = "SELECT playlist.id, name, creator, count(username) AS LikeNumber "
			+ "FROM (playlist LEFT JOIN likePl on (playlist.id=likePl.idPlaylist))"
			+ "GROUP BY playlist.id, name, creator ORDER BY LikeNumber DESC";
	private static final String GET_PLAYLIST_QUERY = "SELECT playlist.id, name, creator, count(username) AS LikeNumber "
			+ "FROM (playlist LEFT JOIN likePl on (playlist.id=likePl.idPlaylist)) " + " WHERE playlist.id =? "
			+ "  GROUP BY playlist.id, name, creator";
	private static final String INSERT_PLAYLIST_QUERY = "INSERT INTO playlist (name, creator) VALUES(?,?)";
	private static final String INSERT_LIKE_QUERY = "INSERT INTO likePl (idPlaylist, username) VALUES(?,?)";

	// ADD SONG TO PL
	private static final String ADD_SONG_QUERY = "UPDATE song_playlist SET pos=pos+1 WHERE pos>= ? AND idPlaylist =?;"
			+ "INSERT INTO song_playlist (idSong, idPlaylist, pos) VALUES(?,?,?)";

	// REMOVE SONG FROM PL
	private static final String VALID_REMOVE_QUERY = "SELECT * FROM song_playlist WHERE idSong =? AND idPlaylist = ? AND pos = ?";

	private static final String REMOVE_SONG_QUERY = "UPDATE song_playlist SET pos=pos-1 WHERE pos> ? AND idPlaylist =?;"
			+ "DELETE FROM song_playlist WHERE idSong =? AND idPlaylist = ?";

	// SORT SONGS IN PL
	private static final String VALID_SORT_QUERY = "SELECT * FROM song_playlist WHERE idSong =? AND idPlaylist = ? AND pos = ? AND"
			+ " (SELECT COUNT(*) FROM song_playlist WHERE idPlaylist = ?) > ?";

	private static final String MOVE_SONG_FORWARD = "UPDATE song_playlist SET pos=pos-1 WHERE pos> ? AND pos<= ? AND idPlaylist =?;"
			+ "UPDATE song_playlist SET pos = ? WHERE idSong=? AND idPlaylist =?";
	private static final String MOVE_SONG_BACK = "UPDATE song_playlist SET pos=pos+1 WHERE pos< ? AND pos>= ? AND idPlaylist =?;"
			+ "UPDATE song_playlist SET pos = ? WHERE idSong=? AND idPlaylist =?";

	public PlaylistDAO() {
	}

	public Optional<Playlist> getPlaylist(int id, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_PLAYLIST_QUERY)) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				Playlist p = null;
				while (result.next()) { // bean setting
					p = new Playlist();
					p.setId(result.getInt(1));
					p.setName(result.getString(2));
					p.setUser(result.getString(3));
					p.setLikes(result.getInt(4));
				}
				return Optional.ofNullable(p);
			}
		}
	}

	public List<Playlist> getAllPlaylists(Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_ALL_QUERY);
				ResultSet result = statement.executeQuery()) {
			List<Playlist> playlists = new ArrayList<Playlist>();
			while (result.next()) {
				Playlist p = new Playlist();
				p.setId(result.getInt(1)); // bean setting
				p.setName(result.getString(2));
				p.setUser(result.getString(3));
				p.setLikes(result.getInt(4));
				playlists.add(p);
			}
			return playlists;
		}
	}

	public boolean createPlaylist(String name, String creator, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(INSERT_PLAYLIST_QUERY)) {
			statement.setString(1, name);
			statement.setString(2, creator);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			if (e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY) // already exists this playlist for this user
				return false;
			else
				throw e; // dao cant handle other SQL errors
		}
	}

	public boolean addLike(int id, String username, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(INSERT_LIKE_QUERY)) {
			statement.setInt(1, id);
			statement.setString(2, username);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			if (e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY) // is already liked
				return false;
			else
				throw e; // dao can't handle other SQL errors
		}
	}

	// ADD SONG TO PL

	public void addSongToPlaylist(int idSong, int idPlaylist, int pos, Connection conn)
			throws SQLException, WrongValueException {
		conn.setAutoCommit(false);
		try (PreparedStatement statement = conn.prepareStatement(ADD_SONG_QUERY)) {
			statement.setInt(1, pos);
			statement.setInt(2, idPlaylist);
			statement.setInt(3, idSong);
			statement.setInt(4, idPlaylist);
			statement.setInt(5, pos);
			statement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			if (e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY) // is already added
				throw new WrongValueException("The song is already in the playlist");
			if (e.getErrorCode() == MysqlErrorNumbers.ER_NO_REFERENCED_ROW_2
					|| e.getErrorCode() == MysqlErrorNumbers.ER_NO_REFERENCED_ROW) // not existing song or playlist
				throw new WrongValueException("The song or the playlist ids are wrong");
			if (e.getErrorCode() == MysqlErrorNumbers.ER_BAD_NULL_ERROR)
				throw new WrongValueException("Invalid Position");

			throw e; // dao can't handle other SQL errors
		} finally {
			conn.setAutoCommit(true); // the connection is in pool
		}
	}

	// REMOVE SONG FROM PL

	public boolean isRemoveSongValid(int idSong, int idPlaylist, int pos, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(VALID_REMOVE_QUERY)) {
			statement.setInt(1, idSong);
			statement.setInt(2, idPlaylist);
			statement.setInt(3, pos);

			try (ResultSet result = statement.executeQuery()) {
				return result.next();
			}
		}
	}

	public void removeSongFromPlaylist(int idSong, int idPlaylist, int pos, Connection conn) throws SQLException {
		conn.setAutoCommit(false);
		try (PreparedStatement statement = conn.prepareStatement(REMOVE_SONG_QUERY)) {
			statement.setInt(1, pos);
			statement.setInt(2, idPlaylist);
			statement.setInt(3, idSong);
			statement.setInt(4, idPlaylist);
			statement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true); // the connection is in pool
		}
	}

	// SORT SONGS IN PL

	public boolean isSortSongValid(int idSong, int idPlaylist, int startPos, int endPos, Connection conn)
			throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(VALID_SORT_QUERY)) {
			statement.setInt(1, idSong);
			statement.setInt(2, idPlaylist);
			statement.setInt(3, startPos);
			statement.setInt(4, idPlaylist);
			statement.setInt(5, endPos);

			try (ResultSet result = statement.executeQuery()) {
				return result.next();
			}
		}
	}

	public void moveSongForward(int idSong, int idPlaylist, int startPos, int endPos, Connection conn)
			throws SQLException {
		moveSong(idSong, idPlaylist, startPos, endPos, conn, MOVE_SONG_FORWARD);
	}

	public void moveSongBack(int idSong, int idPlaylist, int startPos, int endPos, Connection conn)
			throws SQLException {
		moveSong(idSong, idPlaylist, startPos, endPos, conn, MOVE_SONG_BACK);
	}

	private void moveSong(int idSong, int idPlaylist, int startPos, int endPos, Connection conn, String query)
			throws SQLException {
		conn.setAutoCommit(false);
		try (PreparedStatement statement = conn.prepareStatement(query)) {
			statement.setInt(1, startPos);
			statement.setInt(2, endPos);
			statement.setInt(3, idPlaylist);
			statement.setInt(4, endPos);
			statement.setInt(5, idSong);
			statement.setInt(6, idPlaylist);
			statement.executeUpdate();
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		} finally {
			conn.setAutoCommit(true); // the connection is in pool
		}
	}
}
