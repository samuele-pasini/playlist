package it.polimi.tiw.playlist.beans;

import java.util.List;

public class Playlist {

	// attributes

	private String name;
	private String user;
	private int likes;
	private int id;
	private List <Song> songs;

	// getters and setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	public List <Song> getSongs() {
		return songs;
	}

	public void setSongs(List <Song> songs) {
		this.songs = songs;
	}

	// constructors

	public Playlist(String name, String user, int likes, int id) { // constructor with parameters
		this.name = name;
		this.user = user;
		this.likes = likes;
		this.id = id;
		songs = null;
	}
	
	public Playlist(String name, String user, int likes, int id, List <Song> songs) { // constructor with parameters
		this.name = name;
		this.user = user;
		this.likes = likes;
		this.id = id;
		this.songs = songs;
	}

	public Playlist() { // constructor without parameters
		this.name = "";
		this.user = "";
		this.likes = 0;
		this.id = 0;
		songs = null;
	}



}
