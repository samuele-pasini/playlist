package it.polimi.tiw.playlist.dao;

public class AdminDAO extends AbstractAccountDAO {

	public AdminDAO() {
	}

	// override primitive operations: Admin account

	@Override
	protected String doGetLoginQuery() {
		return "SELECT EXISTS (SELECT * FROM admin WHERE BINARY username = ? AND password = ?)";
	}

	@Override
	protected String doGetValidQuery() {
		return "SELECT EXISTS (SELECT * FROM admin WHERE BINARY username = ?)";
	}

	@Override
	protected String doGetSigninQuery() {
		// Signin is undefined for Admin Accounts
		return null;
	}

}
