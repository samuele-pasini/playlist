package it.polimi.tiw.playlist.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.playlist.dao.AdminDAO;
import it.polimi.tiw.playlist.dao.UserDAO;
import it.polimi.tiw.playlist.enums.Role;
import it.polimi.tiw.playlist.utils.Cypher;
import it.polimi.tiw.playlist.utils.JwtUtils;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/do_login")
@MultipartConfig
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AdminDAO adminDAO;
	private UserDAO userDAO;

	@Override
	public void init() throws ServletException {
		super.init();
		adminDAO = new AdminDAO();
		userDAO = new UserDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("username");
		String password = Cypher.hashSHA(request.getParameter("password"));
		if (name == null || password == null || name.isEmpty() || password.isEmpty()) { // invalid params
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("Credentials must be not null");
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			if (userDAO.login(name, password, conn)) {
				response.setStatus(HttpServletResponse.SC_OK); // try to log as user
				response.getWriter().write(JwtUtils.createToken(name, Role.USER));
				// setting User Login Token
			} else if (adminDAO.login(name, password, conn)) {
				response.setStatus(HttpServletResponse.SC_OK); // try to log as admin
				response.getWriter().write(JwtUtils.createToken(name, Role.ADMIN));
				// setting Admin Login Token
			} else {
				// false = login failed
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.getWriter().println("Incorrect credentials");
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to log in now");
			return;
		}

	}

}
