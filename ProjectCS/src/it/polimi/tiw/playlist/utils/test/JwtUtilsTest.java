package it.polimi.tiw.playlist.utils.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.polimi.tiw.playlist.enums.Role;
import it.polimi.tiw.playlist.utils.JwtUtils;

public class JwtUtilsTest {

	public static Key SECRET_KEY;

	@BeforeClass
	public static void setUp() {
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("jwt.properties");
			properties.load(input);
			SECRET_KEY = Keys.hmacShaKeyFor(Base64.getDecoder().decode(properties.getProperty("secretKey"))); // secret
																												// key
																												// reading
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Test
	public void generationTest() {
		String jws = JwtUtils.createToken("tratt", Role.USER);
		// System.out.println(jws);
		assertTrue(JwtUtils.isValid(jws));
	}

	@Test
	public void falsificationTest() {
		assertFalse(JwtUtils.isValid(
				"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmF0dCIsInJvbGUiOiJVU0VSIiwiaWF0IjoxNTkxMTAyNTc0LCJleHAiOjE1OTE5NjY1NzG9.3pYg7ewJutZ8xy32OOCrYEcCfb0pZk_I1On-XiCH8ek"));

	}

	@Test
	public void getRoleTest() {
		assertEquals(JwtUtils.getRole(
				"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmF0dCIsInJvbGUiOiJVU0VSIiwiaWF0IjoxNTkxMTAyNjkzLCJleHAiOjE1OTE5NjY2OTN9.QBS06L3jZCZAzc1oMaim4HlkXs6GgjCl_u5rfRwg2I0")
				.get().toString(), Role.USER.toString());
	}

	@Test
	public void expirationTest() {
		String jws = Jwts.builder().setSubject("tratt") // username setting
				.claim("role", Role.ADMIN) // role setting
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() - 1)) // 10 days
				.signWith(SECRET_KEY).compact();
		assertFalse(JwtUtils.isValid(jws));
	}
}