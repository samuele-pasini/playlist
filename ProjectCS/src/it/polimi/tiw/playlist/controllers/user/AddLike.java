package it.polimi.tiw.playlist.controllers.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.playlist.dao.PlaylistDAO;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/user/do_addLike")
public class AddLike extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PlaylistDAO playlistDAO;

	@Override
	public void init() throws ServletException {
		super.init();
		playlistDAO = new PlaylistDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int playlistId = 0;

		// Check if the playlist id is not null and is a number, if not we'll send an
		// error.
		try {
			playlistId = Integer.parseInt(request.getParameter("id")); // int conversion
		} catch (NullPointerException | NumberFormatException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println("You didn't give a playlist id or it was badly formatted");
			return;
		}

		String name = (String) request.getAttribute("username");

		// Try to add the like.
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			if (playlistDAO.addLike(playlistId, name, conn))
				response.setStatus(HttpServletResponse.SC_OK);
			else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("You already liked this playlist");
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
