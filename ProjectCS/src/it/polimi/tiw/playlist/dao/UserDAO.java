package it.polimi.tiw.playlist.dao;

public class UserDAO extends AbstractAccountDAO {

	public UserDAO() {
	}

	// override primitive operations: User Account

	@Override
	protected String doGetLoginQuery() {
		return "SELECT EXISTS (SELECT * FROM user WHERE BINARY username = ? AND password = ?)";
	}

	@Override
	protected String doGetValidQuery() {
		return "SELECT EXISTS (SELECT * FROM user WHERE BINARY username = ?)";
	}

	@Override
	protected String doGetSigninQuery() {
		return "INSERT INTO user(username,password) VALUES (?,?)";
	}

}
