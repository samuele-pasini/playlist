package it.polimi.tiw.playlist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.mysql.cj.exceptions.MysqlErrorNumbers;

import it.polimi.tiw.exceptions.InvalidReferenceException;
import it.polimi.tiw.playlist.beans.Album;
import it.polimi.tiw.playlist.beans.Song;

public class SongDAO {

	// queries
	private static final String INSERT_QUERY = "INSERT INTO song (title, idAlbum, length, writer, loader, link) VALUES(?,?,?,?,?,?)";
	private static final String GET_SONG_QUERY = "SELECT * FROM song WHERE id =?";
	private static final String GET_ALL_SONGS_QUERY = "SELECT * FROM song ORDER BY writer ASC";
	private static final String GET_ALBUM_SONGS_QUERY = "SELECT * FROM song WHERE idAlbum = ?";
	private static final String GET_PLAYLIST_SONGS_QUERY = "SELECT * FROM song JOIN song_playlist on song.id = song_playlist.idSong WHERE song_playlist.idPlaylist=? ORDER BY pos";
	private static final String GET_NOT_IN_PLAYLIST_QUERY = "SELECT * FROM song "
			+ "WHERE song.id NOT IN (SELECT song.id FROM song JOIN song_playlist on song.id = song_playlist.idSong WHERE song_playlist.idPlaylist = ? )";

	private AlbumDAO albumDAO;

	public SongDAO() {
		albumDAO = new AlbumDAO();
	}

	public Optional<Song> getSong(int id, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_SONG_QUERY)) {
			statement.setInt(1, id);
			Song s = null;
			try (ResultSet result = statement.executeQuery()) {
				s = new Song();
				while (result.next()) { // bean setting
					try {
						s.setId(result.getInt(1));
						s.setTitle(result.getString(2));
						s.setAlbum(
								albumDAO.getAlbum(result.getInt(3), conn).orElseThrow(InvalidReferenceException::new)); // getting
																														// album
																														// from
																														// AlbumDAO
						s.setLength((result.getTime(4).toLocalTime()));
						s.setWriter(result.getString(5));
						s.setLoader(result.getString(6));
						s.setLink(result.getString(7));
					} catch (InvalidReferenceException e) { // invalid album reference
						return Optional.empty();
					}

				}
				return Optional.of(s);
			}
		}
	}

	public List<Song> getAllSongs(Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_ALL_SONGS_QUERY);
				ResultSet result = statement.executeQuery()) {
			List<Song> songs = new ArrayList<Song>();
			while (result.next()) {
				Song s = new Song();
				s.setId(result.getInt(1)); // bean setting
				s.setTitle(result.getString(2));
				Optional<Album> album = albumDAO.getAlbum(result.getInt(3), conn); // getting album from AlbumDAO
				album.ifPresent(s::setAlbum);
				s.setLength((result.getTime(4).toLocalTime()));
				s.setWriter(result.getString(5));
				s.setLoader(result.getString(6));
				s.setLink(result.getString(7));
				if (album.isPresent()) // cannot add with invalid album
					songs.add(s);
			}
			return songs;

		}
	}

	public List<Song> getAlbumSongs(int idAlbum, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_ALBUM_SONGS_QUERY)) {
			statement.setInt(1, idAlbum);
			try (ResultSet result = statement.executeQuery()) {
				List<Song> songs = new ArrayList<Song>();
				while (result.next()) {
					Song s = new Song();
					s.setId(result.getInt(1)); // bean setting
					s.setTitle(result.getString(2));
					Optional<Album> album = albumDAO.getAlbum(result.getInt(3), conn); // getting album from AlbumDAO
					album.ifPresent(s::setAlbum);
					s.setLength((result.getTime(4).toLocalTime()));
					s.setWriter(result.getString(5));
					s.setLoader(result.getString(6));
					s.setLink(result.getString(7));
					if (album.isPresent()) // cannot add with invalid album
						songs.add(s);
				}
				return songs;
			}
		}
	}

	public List<Song> getPlaylistSongs(int idPlaylist, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_PLAYLIST_SONGS_QUERY)) {
			statement.setInt(1, idPlaylist);
			try (ResultSet result = statement.executeQuery()) {
				List<Song> songs = new ArrayList<Song>();
				while (result.next()) {
					Song s = new Song();
					s.setId(result.getInt(1)); // bean setting
					s.setTitle(result.getString(2));
					Optional<Album> album = albumDAO.getAlbum(result.getInt(3), conn); // getting album from AlbumDAO
					album.ifPresent(s::setAlbum);
					s.setLength((result.getTime(4).toLocalTime()));
					s.setWriter(result.getString(5));
					s.setLoader(result.getString(6));
					s.setLink(result.getString(7));
					if (album.isPresent()) // cannot add with invalid album
						songs.add(s);
				}
				return songs;
			}
		}
	}

	public List<Song> getNotInPlaylistSongs(int idPlaylist, Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(GET_NOT_IN_PLAYLIST_QUERY)) {
			statement.setInt(1, idPlaylist);
			try (ResultSet result = statement.executeQuery()) {
				List<Song> songs = new ArrayList<Song>();
				while (result.next()) {
					Song s = new Song();
					s.setId(result.getInt(1)); // bean setting
					s.setTitle(result.getString(2));
					Optional<Album> album = albumDAO.getAlbum(result.getInt(3), conn); // getting album from AlbumDAO
					album.ifPresent(s::setAlbum);
					s.setLength((result.getTime(4).toLocalTime()));
					s.setWriter(result.getString(5));
					s.setLoader(result.getString(6));
					s.setLink(result.getString(7));
					if (album.isPresent()) // cannot add with invalid album
						songs.add(s);
				}
				return songs;
			}
		}
	}

	public boolean addSong(String title, String writer, LocalTime length, int idAlbum, String loader, String link,
			Connection conn) throws SQLException {
		try (PreparedStatement statement = conn.prepareStatement(INSERT_QUERY)) {
			statement.setString(1, title);
			statement.setInt(2, idAlbum);
			statement.setTime(3, Time.valueOf(length));
			statement.setString(4, writer);
			statement.setString(5, loader);
			statement.setString(6, link);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			if (e.getErrorCode() == MysqlErrorNumbers.ER_DUP_ENTRY // duplicate title + idAlbum (UNIQUE couple)
					|| e.getErrorCode() == MysqlErrorNumbers.ER_NO_REFERENCED_ROW_2
					|| e.getErrorCode() == MysqlErrorNumbers.ER_NO_REFERENCED_ROW) // invalide album reference
				return false; // add operation failed
			else
				throw e; // dao cant handle other SQL errors
		}
	}

	/**
	 * Overload with Album bean parameter
	 */
	public boolean addSong(String title, String writer, LocalTime length, Album album, String loader, String link,
			Connection conn) throws SQLException {
		return addSong(title, writer, length, album.getId(), loader, link, conn);
	}
}
