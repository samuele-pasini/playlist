package it.polimi.tiw.playlist.controllers.admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.polimi.tiw.exceptions.WrongValueException;
import it.polimi.tiw.playlist.dao.SongDAO;
import it.polimi.tiw.playlist.utils.CheckUtils;
import it.polimi.tiw.playlist.utils.pool.ConnectionPool;

@WebServlet("/admin/do_addSong")
@MultipartConfig
public class AddSong extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SongDAO songDAO = null;

	@Override
	public void init() throws ServletException {
		super.init();
		songDAO = new SongDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String length = request.getParameter("length");
		String link = request.getParameter("link");
		String albumS = request.getParameter("album");
		
		try {
			// Allows the user to insert the song's length with the "mm:ss" or "m:ss"
			// format.
			length = (length.length() == 4) ? "00:0" + length : "00:" + length;
			CheckUtils.validateSong(title, writer, length, albumS, link); // validation
		
		} catch (WrongValueException e) { // validation failed
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(e.getMessage());
			return;
		}
		try (Connection conn = ConnectionPool.getIstance().getConnection()) {
			int idAlbum = Integer.parseInt(albumS); // safer for validateSongControl
			LocalTime timeLength = LocalTime.parse(length);
			if (songDAO.addSong(title, writer, timeLength, idAlbum, (String) request.getAttribute("username"), link,
					conn)) {
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().write("Succesfully added");
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("This Song already exists");
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("It's not possible to add songs now");
		}
	}
}
